// Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.


function cacheFunction(cb) {
    let cache = {}
    return function (num){
        if(!cache[num]){
            cache[num] = cb(num)
            return cache[num]
        }
        else{
            console.log("extracted from cache obj")
            return cache[num]

        }
    }
    
}

export {cacheFunction}












